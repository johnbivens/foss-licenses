# FOSS licenses

This repository contains a presentation providing an overview of FOSS licenses,
originally given at the [Software DevMeetup Fulda](https://www.meetup.com/de-DE/Software-DevMeetup-Fulda/)
in Fulda, Germany on 05.04.2018.

The presentation itself is written in LaTeX using the [Beamer](https://en.wikipedia.org/wiki/Beamer_(LaTeX))
class and should be compiled with [`PDFLaTeX`](https://www.tug.org/applications/pdftex/).

A PDF version of the presentation may be found [here](https://gitlab.com/johnbivens/foss-licenses/blob/master/foss-licenses_de.pdf).
