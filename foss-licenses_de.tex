% Copyright 2018, John Bivens
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

\PassOptionsToPackage{table}{xcolor}

% The handout option will ignore pauses and collapse split frames
% into single frames, which is useful when generating a printer-
% friendly PDF. Remove the handout option when generating a PDF
% for presentation purposes.
% \documentclass[aspectratio=169,10pt]{beamer}
\documentclass[handout,aspectratio=169,10pt]{beamer}

\usetheme{Copenhagen}
\usecolortheme{beetle}
\useinnertheme{rectangles}
\useoutertheme{miniframes}
\useoutertheme{smoothbars}
\usepackage{color}
\definecolor{bg}{RGB}{153,153,153}
\definecolor{urblue}{RGB}{0,70,127}
\definecolor{secondblue}{RGB}{61,76,121}
\setbeamercolor{background canvas}{bg=white}
\setbeamercolor{structure}{bg=gray!30, fg=secondblue}
\setbeamercolor{block title}{bg=gray!20}
\setbeamercolor{block body}{bg=gray!10}
\setbeamertemplate{bibliography item}{\insertbiblabel}
\setbeamertemplate{caption}[numbered]
\urlstyle{same}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[ngerman]{babel}
\usepackage[
    left = \glqq,% 
    right = \grqq,% 
    leftsub = \glq,% 
    rightsub = \grq%
]{dirtytalk}
\usepackage[sfdefault]{AlegreyaSans}
\usepackage{booktabs}
\usepackage[labelfont={sc,color=secondblue}]{caption}
\usepackage{soul}
\usepackage{multicol}
\usepackage{adjustbox}
\usepackage{pifont}

% Show navigation circles for frames outside of a subsection
\usepackage{remreset}
\makeatletter
\@removefromreset{subsection}{section}
\makeatother
\setcounter{subsection}{1}

\graphicspath{ {./resources/} }

\author{John Bivens}
\institute{Software DevMeetup Fulda}
\title{FOSS-Lizenzen}
\subtitle{eine Übersicht}
\date{05.04.2018}

\begin{document}

\AtBeginSection[]
{
	\begin{frame}<beamer>
		\tableofcontents[currentsection,hideothersubsections]
	\end{frame}
}

\begin{frame}
	\titlepage
\end{frame}

\begin{frame}
	\frametitle{Worum geht's?}
	\tableofcontents[hideothersubsections]
\end{frame}

\section{Das Kleingedruckte}

\begin{frame}
	\frametitle{Eines vorweg: IANAL!}
	\centering
	\onslide<+->{I am \textbf{not} a lawyer!}
	\pause[\thebeamerpauses]
	\begin{figure}[h]
		\includegraphics[scale=0.25]{ianal}
		\caption{Keine Haftung für Schäden, schwere Verletzungen oder ggf. Todesfälle. \cite{ianal}}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Wer bin ich?}
	\begin{itemize}[<+->]
		\item John Bivens
		\item Software Engineer seit 2007; seit 2014 bei PROEMION GmbH in Fulda
		\item Interessen und Schwerpunkte
		\begin{itemize}[<.->]
			\item Free Software
			\item GNU/Linux-basierte Softwareentwicklung
			\item Qt
			\item C++
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Diese Präsentation ist FOSS!}
	Diese Präsentation wurde ausschließlich mit freier Software erstellt und unterliegt der GPLv3. Der Quellcode ist auf GitLab zu finden: \\
	\bigskip
	\centering
	\url{https://gitlab.com/johnbivens/foss-licenses}
\end{frame}

\section{Was ist FOSS?}

\begin{frame}
	\frametitle{Free as in \say{Freedom}...}
	\textbf{FOSS}: \textit{Free and Open Source Software} \\
	\bigskip
	Das \say{F} spielt eine wesentliche Rolle -- die \textit{Freiheit} der Software \textit{und deren Nutzer} steht gezielt im Vordergrund.
\end{frame}

\begin{frame}
    \frametitle{...und \say{free as in beer}?}
	\onslide<+->{\textbf{FOSS} ist ein Versuch, den zweideutigen Begriff \say{Free Software} etwas eindeutiger zu machen, da das englische Wort \say{free} zweideutig und somit problematisch ist. \\}
	\pause[\thebeamerpauses]
	\bigskip
	In diesem Fall gilt:
    \bigskip
    \begin{itemize}
        \item Free $=$ frei (\say{free as in \say{freedom}})
        \item Free $\neq$ gratis (\say{free as in \say{free beer}})
    \end{itemize}
    \bigskip
    \pause
	\textit{Dennoch}: In den meisten Fällen ist Free Software gleichzeitig \st{free} \textit{gratis}.
\end{frame}

\begin{frame}
    \frametitle{Und was ist FLOSS?!}
    \textbf{FLOSS}: \textit{Free/Libre and Open Source Software}
    \bigskip
    \begin{itemize}
    	\item FLOSS $=$ FOSS
    \end{itemize}
    \bigskip
    \pause
    Diese Variante ist lediglich ein \textit{weiterer} Versuch, die Zweideutigkeit von \say{Free} mithilfe vom eindeutigeren \say{Libre} (spanisch, französisch) zu umgehen.
\end{frame}

\begin{frame}
	\frametitle{Was bedeutet \say{free}?}
	\onslide<+->{Laut der Free Software Foundation (FSF) muss Free Software vier Freiheiten gewährleisten. \cite{freiesoftwarewasistdas}}
	\bigskip
	\begin{itemize}[<+->]
		\item \textit{Freiheit 0}: Die Freiheit, das Programm auszuführen wie man möchte, für jeden Zweck.
		\item \textit{Freiheit 1}: Die Freiheit, die Funktionsweise des Programms zu untersuchen und eigenen Datenverarbeitungbedürfnissen anzupassen.
		\item \textit{Freiheit 2}: Die Freiheit, das Programm zu redistribuieren und damit seinen Mitmenschen zu helfen.
		\item \textit{Freiheit 3}: Die Freiheit, das Programm zu verbessern und diese Verbesserungen der Öffentlichkeit freizugeben, damit die gesamte Gesellschaft davon profitiert.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Vorsicht: \say{Open Source} \textit{kann} etwas ganz anderes sein}
	\centering
	Ein Quadrat ist ein Rechteck, aber ein Rechteck ist nicht unbedingt ein Quadrat. \\
	\bigskip
	\textit{ähnlich}: \\
	\bigskip
	Free Software ist open-source, aber Open Source Software ist nicht unbedingt free.
\end{frame}

\section{Kurzer Geschichtsunterricht}

\begin{frame}
	\frametitle{Software bis ca. 1983 und Angriff der EULAs}
	\onslide<+->{Software-Lizenzen und End-User License Agreements (EULAs) existierten anfangs nicht. \cite{wikipediafreesoftwarelicense}}
	\bigskip
	\begin{itemize}[<+->]
		\item bis 1974 galt das US-amerikanische Urheberrecht \textit{nicht} für Software
		\item Software unterlag \textit{keinen} oder nur \textit{sehr informellen} Lizenzen
		\item bis Einführung des Urheberrechts unterlag Software der Gemeinfreiheit (Public Domain) 
		\item ab ca. 1983 (u.a. Rechtsfall \textit{Apple v. Franklin}) durfte Urheberrecht auf Software angewandt werden
	\end{itemize}
\end{frame}

\section{FOSS-Lizenzen}

\begin{frame}
	\frametitle{Um welche Lizenzen geht es heute?}
	\onslide<+->{Als Basis dienen Statistiken von GitHub -- 2015 wurden folgende FOSS-Lizenzen am häufigsten verwendet \cite{githublicensestats}:}
	\bigskip
	\begin{table}
		\adjustbox{max height=\dimexpr\textheight-5.5cm\relax,max width=\textwidth}
    	{
    		\rowcolors{2}{gray!25}{white}
			\begin{tabular}{l l r}
				\hline
				\rowcolor{gray!50}
				Rang & Lizenz & Repositories in Prozent \\
				\hline \hline
				1 & MIT & 44,69$\%$ \\
				2 & (sonstige) & 15,68$\%$ \\
				3 & GPLv2 & 12,96$\%$ \\
				4 & Apache & 11,19$\%$ \\
				5 & GPLv3 & 8,88$\%$ \\
				6 & BSD 3-clause & 4,53$\%$ \\
				7 & (keine) & 1,87$\%$ \\
				8 & BSD 2-clause & 1,70$\%$ \\
				9 & LGPLv3 & 1,30$\%$ \\
				10 & AGPLv3 & 1,05$\%$ \\
				\hline	
			\end{tabular}
		}
		\caption{Verbreitung von FOSS-Lizenzen auf GitHub.com (2015).}
	\end{table}
\end{frame}

\begin{frame}
	\frametitle{Eine kleine Rant für Zwischendurch...}
	\texttt{<rant>} \\
	GitHub ist proprietär und sollte lieber vermieden werden, denn FOSS-Alternativen gibt es genug. \\
	Erzählt das bitte den rund 26 Millionen Nutzern... \\
	\texttt{</rant>} \\
	\bigskip
	\centering
	\includegraphics[scale=0.25]{troll}
\end{frame}

\begin{frame}
	\frametitle{Freie Lizenzmodelle}
	Grundsätzlich gibt es zwei Arten von freien Lizenzen:
	\bigskip
	\begin{itemize}
		\item Copyleft-Lizenzen
		\item \say{freizügige} Lizenzen
	\end{itemize}
\end{frame}

\subsection{Copyleft-Lizenzen}

\begin{frame}
	\frametitle{Geniestreich Copyleft}
	\onslide<+->{\textbf{Copyleft} ist nicht nur ein Wortspiel!}
	\begin{itemize}[<+->]
		\item schlauer Einsatz des bestehenden Urheberrechtes (\textit{Copyright}), wodurch die Freiheit einer Copyleft-lizenzierten Software sichergestellt wird
		\item stellt bei der Weitergabe der Software sicher, dass alle ursprünglich gegebenen Freiheiten erhalten bleiben
		\item Copyleft-Lizenzen haben somit einen gewissen \say{viralen} Effekt
		\item Erste Verwendung: \textit{The GNU Manifesto} (Richard Stallman, \textbf{1985}) \cite{gnumanifesto}
	\end{itemize}
	\bigskip
	\pause[\thebeamerpauses]
	\begin{center}
		\begin{figure}[h]
			\includegraphics[scale=0.25]{copyleft}
			\caption{Copyleft stellt Copyright wortwörtlich auf den Kopf. \cite{copyleftlogo}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Die ersten Copyleft-Lizenzen: Vorgänger der GPL}
	In den 1980ern setzte das GNU-Projekt Copyleft-Lizenzen bei GNU Emacs und der GNU Compiler Collection (GCC) ein. \cite{wikipediafreesoftwarelicense} Zu diesem Zeitpunkt gab es noch keine GPL-Lizenz.
\end{frame}

\begin{frame}
	\frametitle{GNU General Public License (GPL)}
	\begin{itemize}[<+->]
	    \item Free Software Foundation, \textbf{1989}
	    \item Vereinheitlichung von sonst inkompatiblen, programmspezifischen Lizenzen des GNU-Projektes (GNU Emacs, GNU Debugger, GNU Compiler Collection) \cite{wikipediagnugpl}
	    \item setzt die Veröffentlichung des Quellcodes bei jeder Weitergabe der Software voraus
	    \item GPL-lizenzierte Software darf nicht mit Software kombiniert werden, die einer restriktiveren Lizenz unterliegt (das Gesamtwerk muss einer Lizenz unterliegen, die \textit{mindestens} alle Voraussetzungen der GPL erfüllt)
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{GNU General Public License Version 2 (GPLv2)}
	\begin{itemize}[<+->]
	    \item Free Software Foundation, \textbf{1991}
	    \item Erweiterung der GPL, hauptsächlich durch die \say{Freiheit oder Tod}-Klausel \cite{wikipediagnugpl}
	    \begin{itemize}
		    \item sollte es aus irgendwelchen Gründen nicht möglich sein, die Bedingungen der GPLv2 zu erfüllen (z.B. Softwarepatente, Gerichtsurteil), darf die Software \textit{überhaupt nicht} verbreitet werden
	    	\item verhindert die Anwendung von Softwarepatenten, die die Freiheit der Software effektiv zerstören könnten
	    \end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{GNU Lesser General Public License Version 2.1 (LGPLv2.1)}
	\begin{itemize}[<+->]
	    \item Free Software Foundation
		\item ursprünglich: GNU Library General Public License (Version 2.0, \textbf{1991})
		\item umbenannt in GNU Lesser General Public License (Version 2.1, \textbf{1999})
	    \item Lockerung der GPLv2, hauptsächlich für Programmbibliotheken
	    \begin{itemize}
		    \item \textit{schwaches Copyleft}: die Programmbibliothek an sich wird durch die LGPLv2.1 geschützt -- Programme, die diese Bibliothek \textit{dynamisch} verlinken, bleiben allerdings unberührt
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{GNU General Public License Version 3 (GPLv3)}
	\begin{itemize}[<+->]
	    \item Free Software Foundation, \textbf{2007}
	    \item Erweiterung der GPLv2 \cite{wikipediagnugpl}
	    \begin{itemize}
	    	\item verbesserte Internationalisierung: weniger auf das US-amerikanische Rechtssystem ausgelegt
	    	\item erweiterter Schutz vor Patenten, DRM
	    	\item verbesserte Kompatibilität zu anderen freien Lizenzen (z.B. Apache License Version 2.0)
	    	\item anti-\say{Tivoization}: bei der Verwendung in \say{\textit{Benutzerprodukten}} ist die Zurverfügungstellung von Hinweisen zur Installation und Modifikation der Software Pflicht (Austauschbarkeit)
	    \end{itemize}
	    \item Erweiterungen werden oft kritisiert; viele bleiben bewusst bei der GPLv2 und wechseln nicht auf GPLv3
	    \item nur bedingt mit der GPLv2 kompatibel (mit Angabe der \say{or later}-Klausel)
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{GNU Lesser General Public License Version 3 (LGPLv3)}
	\begin{itemize}[<+->]
	    \item Free Software Foundation, \textbf{2007}
	    \item Lockerung der GPLv3 (siehe LGPLv2.1), hauptsächlich für Programmbibliotheken
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Affero General Public License Version 3 (AGPLv3)}
	\begin{itemize}[<+->]
	    \item Free Software Foundation, \textbf{2007}
	    \item fast identisch zu der GPLv3, aber besonders interessant für Web-Anwendungen
	    \item schließt das sogenannte \say{Application Service Provider} (ASP) Schlupfloch, wodurch Copyleft umgangen werden kann (genau genommen wird die Software auf einem Server \textit{verwendet}, nicht \textit{weitergegeben}) \cite{wikipediagnuagpl}
	    \item bei der Verwendung von AGPLv3-lizenzierten Software muss der Quellcode (und ggf. Modifizierungen) zur Verfügung gestellt werden 
	    \item Nachfolger der GPLv2-basierte Affero General Public License (AGPLv1) (Affero, Inc., \textbf{2002})
	\end{itemize}
\end{frame}

\subsection{Freizügige Lizenzen}

\begin{frame}
	\frametitle{Was sind \say{freizügige} Lizenzen?}
	\onslide<+->{Die sogenannten \say{freizügigen} Lizenzen sind FOSS-Lizenzen, die die Verwendung einer kompatiblen Lizenz bei Weitergabe der Software \textit{nicht} voraussetzen.}
	\bigskip
	\pause[\thebeamerpauses]
	\begin{itemize}[<+->]
		\item wichtigster Unterschied gegenüber Copyleft: \textit{kein} \say{viraler} Effekt
		\item die Freiheit der Software wird somit \textit{nicht} explizit geschützt
		\item für Unternehmen und kommerzielle Zwecke besonders vorteilhaft (nicht zur Veröffentlichung von Modifizierungen verpflichtet)
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{BSD 4-clause License}
	\onslide<+->{Die erste BSD-Lizenz (\say{BSD 4-clause License}, auch als \say{BSD-old} oder \say{BSD Original} bekannt) wurde \textbf{1988} von den Regents of the University of California angewandt. \cite{bsdlicenses}}
	\bigskip
	\pause[\thebeamerpauses]
	\begin{itemize}[<+->]
		\item \textit{Klausel 1}: Die Weitergabe des Quellcodes setzt die Weitergabe des ursprünglichen Urheberrechtshinweises, der vier Klauseln und des Haftungsausschlusses voraus.
		\item \textit{Klausel 2}: Die Weitergabe des Maschinencodes ist nur wie im ersten Klausel zulässig (z.B. Teil der Dokumentation oder des Benutzerhandbuches).
		\item \textit{Klausel 3}: Alle Werbematerialien, die die Features oder den Einsatz der Software erwähnen, müssen folgende Bemerkung enthalten: \say{This product includes software developed by \textit{Autor}.}
		\item \textit{Klausel 4}: Ohne schriftliche Zustimmung dürfen die Namen der Autoren nicht zwecks Befürwortung des Produktes verwendet werden.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{BSD 3-clause License}
	\begin{itemize}[<+->]
		\item Regents of the University of California, \textbf{1999}
		\item Entfernung der umstrittenen Klausel zu Werbematerialien, sonst identisch zu BSD 4-clause
		\item auch bekannt als: \say{BSD License 2.0}, \say{BSD-new}, \say{New BSD License}, \say{Modified BSD License}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{BSD 2-clause License}
	\begin{itemize}[<+->]
		\item The FreeBSD Project, \textbf{1999}
		\item Entfernung der \say{Befürwortungsklausel}, sonst identisch zu BSD 3-clause
		\item auch bekannt als: \say{Simplified BSD License}, \say{FreeBSD License}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{MIT License}
	\begin{itemize}[<+->]
		\item Massachusetts Institute of Technology, \textbf{1988}
		\item Angabe des Urheberrechtsvermerks und des Erlaubnisvermerks wird bei Weitergabe vorausgesetzt
		\item auch bekannt als: \say{X11 License}, \say{Expat License} (\textit{Achtung}: hier gibt es teilweise kleine Unterschiede, die Namen sind nicht immer eindeutig!)
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Apache License 2.0}
	\begin{itemize}[<+->]
		\item Apache Software Foundation, \textbf{2004}
		\item Angabe des Urheberrechtsvermerks und des Erlaubnisvermerks wird bei Weitergabe vorausgesetzt
		\item Modifizierungen der Software müssen explizit gekennzeichnet werden
		\item Klausel zur Verhinderung von Softwarepatenten
	\end{itemize}
\end{frame}

\section{Compliance}

\begin{frame}
	\frametitle{Bitte an die Lizenzbedingungen denken!}
	\onslide<+->{FOSS-Lizenzen bieten sehr zwar viele Freiheiten, aber deren Verwendung ist trotzdem an sehr konkreten Bedingungen gekoppelt. \\}
	\bigskip
	\pause[\thebeamerpauses]
	\centering
	\textbf{Die Bedingungen von FOSS-Lizenzen sind nicht zu missachten und \underline{müssen} erfüllt werden!}
\end{frame}

\begin{frame}
	\frametitle{Übersicht einiger Lizenzbedingungen}
	\centering
	\begin{table}
		\adjustbox{max height=\dimexpr\textheight-5.5cm\relax,max width=\textwidth}
    	{
    		\rowcolors{4}{gray!25}{white}
			\begin{tabular}{l c c c c c c c c c c c}
				\cmidrule(r){2-7}
				\cmidrule(r){8-12}
				& \multicolumn{6}{c}{Copyleft} & \multicolumn{5}{c}{freizügig} \\
				\cmidrule(r){2-7}
				\cmidrule(r){8-12}
				\hline
				\rowcolor{gray!50}
				& GPL & GPLv2 & LGPLv2.1 & GPLv3 & LGPLv3 & AGPLv3 & BSD 4-clause & BSD 3-clause & BSD 2-clause & MIT & Apache 2.0 \\
				\hline \hline
				Lizenztext beilegen & \ding{51} & \ding{51} & \ding{51} & \ding{51} & \ding{51} & \ding{51} & \ding{51} & \ding{51} & \ding{51} & \ding{51} & \ding{51} \\
				Urheberrechtsvermerk beilegen & \ding{51} & \ding{51} & \ding{51} & \ding{51} & \ding{51} & \ding{51} & \ding{51} & \ding{51} & \ding{51} & \ding{51} & \ding{51} \\
				Modifizierungen als Quellcode veröffentlichen & \ding{51} & \ding{51} & \ding{51} & \ding{51} & \ding{51} & \ding{51} & & & & & \\
				Original-Quellcode beilegen & \ding{51} & \ding{51} & \ding{51} & \ding{51} & \ding{51} & & & & & & \\
				Austauschbarkeit gewährleisten (Installationshinweise) & & & & \ding{51} & \ding{51} & & & & & & \\
				Attribution/Notice & & & & & \ding{51} & & & & & & \ding{51} \\
				Hinweis in Werbematerialien & & & & & & & \ding{51} & & & & \\
				\hline	
			\end{tabular}
		}
		\caption{Bedingungen ausgewählter FOSS-Lizenzen.}
	\end{table}
\end{frame}

\begin{frame}
	\frametitle{Compliance: unter Umständen eine Mammutaufgabe!}
	\onslide<+->{In komplexen Systemen kann die Erfüllung von Lizenzbedingungen schnell problematisch werden.}
	\bigskip
	\pause[\thebeamerpauses]
	\begin{itemize}[<+->]
		\item Erkennung von Softwarepaketen, Modulen und Lizenzen
		\begin{itemize}
			\item Was wird überhaupt eingesetzt, und in welcher Form?
			\item Welche Lizenzen sind zu beachten?
			\item Welche Lizenzbedingungen müssen erfüllt werden?
		\end{itemize}
		\item Mischung von diversen Lizenzen und Kompatibilität zwischen den Lizenzen
		\item Einsatz von mehreren Lizenzen bei einzelnen Softwarepaketen
		\begin{itemize}
			\item Beispiel: \texttt{ppp}: ja nach Verwendung könnten GPLv2, LGPLv2.1, BSD 4-clause und/oder BSD 3-clause in Frage kommen oder von Bedeutung sein (!!!)
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Tools zur Compliance}
	\onslide<+->{Mittlerweile gibt es einige Tools, die die Bewältigung dieser komplexen Aufgabe erleichtern könnten.}
	\bigskip
	\pause[\thebeamerpauses]
	\begin{itemize}[<+->]
		\item \href{https://www.fossology.org/}{FOSSology} (The Linux Foundation, GPLv2/LGPLv2.1)
		\item \href{https://www.codeaurora.org/project/qualcomm-ostg-lid}{Qualcomm OSTG LiD} (The Linux Foundation, BSD 3-clause)
		\item \href{https://spdx.org/}{Software Package Data Exchange (SPDX, Standardisierung)} (The Linux Foundation)
		\item \href{https://github.com/dl9pf/meta-spdxscanner}{meta-spdxscanner} (Yocto Meta-Layer mit SPDX-Scanner) (Jan-Simon Möller, MIT)
		\item \href{https://github.com/openembedded/openembedded-core/blob/master/meta/classes/spdx.bbclass}{Yocto+SPDX} (\texttt{spdx.bbclass}) (MIT?)
		\item \href{https://www.blackducksoftware.com/products}{Black Duck Suite} (Black Duck Software, Inc., proprietär)
	\end{itemize}
	\bigskip
	\pause[\thebeamerpauses]
	\textbf{Dennoch gilt}: \textit{Der Rat eines Anwaltes bzw. einer Anwältin sollte immer eingeholt werden}!
\end{frame}

\section{Fragen und Antworten}

\begin{frame}
	\frametitle{Fragen und Antworten}
	\begin{center}
		\textbf{Fragen?} \\
		(...und hoffentlich Antworten!)
	\end{center}
\end{frame}

\section{Quellen}

\begin{frame}
	\frametitle{Quellen}
	\begin{thebibliography}{9}
		\bibitem{ianal} imgflip.com. \url{https://i.imgflip.com/m5h7a.jpg}	
		\bibitem{freiesoftwarewasistdas} GNU-Projekt. \href{https://www.gnu.org/philosophy/free-sw.de.html}{\textit{Freie Software. Was ist das?}}.
		\bibitem{wikipediafreesoftwarelicense} Wikipedia. \href{https://en.wikipedia.org/wiki/Free_software_license}{\textit{Free software license}}.
		\bibitem{githublicensestats} GitHub. \href{https://blog.github.com/2015-03-09-open-source-license-usage-on-github-com/}{\textit{Open source license usage on GitHub.com}}.
		\bibitem{gnumanifesto} Stallman, Richard. \href{https://www.gnu.org/gnu/manifesto.html}{\textit{The GNU Manifesto}}.
		\bibitem{copyleftlogo} Zscout370 und Sertion. \url{https://commons.wikimedia.org/wiki/File:Copyleft.svg}
		\bibitem{wikipediagnugpl} Wikipedia. \href{https://de.wikipedia.org/wiki/GNU_General_Public_License}{\textit{GNU General Public License}}.
		\bibitem{wikipediagnuagpl} Wikipedia. \href{https://de.wikipedia.org/wiki/GNU_Affero_General_Public_License}{\textit{GNU Affero General Public License}}.
		\bibitem{bsdlicenses} Wikipedia. \href{https://en.wikipedia.org/wiki/BSD_licenses}{\textit{BSD licenses}}.
	\end{thebibliography}
\end{frame}

\end{document}
